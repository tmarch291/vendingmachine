package com.timsnas.vendingmachine.entities;

public class Coin {
	
	public double diameter;
	public double weight;
	public int value;
	
	public Coin(double diameter, double weight, int value)
	{
		this.diameter = diameter;
		this.weight = weight;
		this.value = value;
	}
	
	public double getDiameter() {
		return diameter;
	}
	public void setDiameter(double diameter) {
		this.diameter = diameter;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}


}

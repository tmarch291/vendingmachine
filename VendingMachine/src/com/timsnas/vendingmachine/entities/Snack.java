package com.timsnas.vendingmachine.entities;

public class Snack {
	
	public String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

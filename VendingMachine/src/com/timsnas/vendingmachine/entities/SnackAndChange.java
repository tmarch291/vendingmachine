package com.timsnas.vendingmachine.entities;

import java.util.List;

public class SnackAndChange {
	
	public Snack snack;
	public List<Coin> coins;
	
	public Snack getSnack() {
		return snack;
	}
	public void setSnack(Snack snack) {
		this.snack = snack;
	}
	public List<Coin> getCoins() {
		return coins;
	}
	public void setCoins(List<Coin> coins) {
		this.coins = coins;
	}

}

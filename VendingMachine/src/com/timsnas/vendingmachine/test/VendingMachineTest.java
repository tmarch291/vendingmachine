package com.timsnas.vendingmachine.test;

import java.util.List;

import com.timsnas.vendingmachine.controllers.SnackController;
import com.timsnas.vendingmachine.controllers.Transaction;
import com.timsnas.vendingmachine.controllers.CoinValidator;
import com.timsnas.vendingmachine.controllers.Transaction;
import com.timsnas.vendingmachine.entities.Coin;
import com.timsnas.vendingmachine.entities.SnackAndChange;

public class VendingMachineTest {

	public static void main(String[] args) {
		
		
		//dime test
		/*
		Coin coin = new Coin(17.91,2.268,0); //simulates the actual diameter & weight of a dime (value unknown)
		
		if (CoinValidator.getCoinValue(coin).getValue() == 10)
		{
			System.out.println("test passed");
		}
		else
		{
			System.out.println("test failed");
		}
		*/
		
		//nickel test
		/*
		Coin coin = new Coin(21.21,5,0); //simulates the actual diameter & weight of a dime (value unknown)
		
		if (CoinValidator.getCoinValue(coin).getValue() == 1)
		{
			System.out.println("test passed");
		}
		else
		{
			System.out.println("test failed");
		}
		*/
		
		//quarter test
		/*
		Coin coin = new Coin(24.26,5.67,0); //simulates the actual diameter & weight of a dime (value unknown)
		
		if (CoinValidator.getCoinValue(coin).getValue() == 25)
		{
			System.out.println("test passed");
		}
		else
		{
			System.out.println("test failed");
		}
		*/
		
		//coinbox test after 50c is inserted
		/*
		Coin coin1 = new Coin(24.26,5.67,0);
		Coin coin2 = new Coin(24.26,5.67,0);
		
		Transaction transaction = new Transaction();
		
		transaction.addCoins(coin1);
		transaction.addCoins(coin2);
		
		if (transaction.getValue() == 50)
		{
			System.out.println("test passed");
		}
		else
		{
			System.out.println("test failed");
		}
		*/
		
		//coinbox test to make change after 50c is inserted
		/*
		Coin quarter = new Coin(24.26,5.67,0);
		Coin dime = new Coin(17.91,2.268,0);
		Coin nickel = new Coin(21.21,5,0);
		
		Transaction transaction = new Transaction();
		
		transaction.addCoins(quarter);
		transaction.addCoins(nickel);
		transaction.addCoins(dime);
		
		List<Coin> coins = transaction.giveChange(0);
		for (int i = 0; i < coins.size(); i++)
		{
			System.out.println("coin value:" + coins.get(i).getValue());
		}
		*/
		//test buying a soda with four quarters
		/*
		Coin quarter = new Coin(24.26,5.67,0);
		Transaction transaction = new Transaction();
		transaction.addCoins(quarter);
		transaction.addCoins(quarter);
		transaction.addCoins(quarter);
		transaction.addCoins(quarter);
		
		SnackController snackController = new SnackController();
		SnackAndChange snackAndChange = snackController.buyCola(transaction);
		
		if (snackAndChange.snack == null)
		{
			System.out.println("not enough coins");
		}
		else
		{
			System.out.println("snack is:" + snackAndChange.getSnack().getName());
			System.out.println("number of coins in change is:" + snackAndChange.getCoins().size());

		}		
		*/
		//test buying a chips with two quarters
		/*
		Coin quarter = new Coin(24.26,5.67,0);
		Transaction transaction = new Transaction();
		transaction.addCoins(quarter);
		transaction.addCoins(quarter);
		
		SnackController snackController = new SnackController();
		SnackAndChange snackAndChange = snackController.buyChips(transaction);
		
		if (snackAndChange.snack == null)
		{
			System.out.println("not enough coins");
		}
		else
		{
			System.out.println("snack is:" + snackAndChange.getSnack().getName());
			System.out.println("number of coins in change is:" + snackAndChange.getCoins().size());

		}	
		*/	
		//test buying a chips with mixed change
		/*
		Coin quarter = new Coin(24.26,5.67,0);
		Coin dime = new Coin(17.91,2.268,0);
		Coin nickel = new Coin(21.21,5,0);
		
		Transaction transaction = new Transaction();
		transaction.addCoins(quarter);
		transaction.addCoins(dime);
		transaction.addCoins(nickel);
		transaction.addCoins(nickel);
		transaction.addCoins(nickel);
		transaction.addCoins(nickel);
		
		SnackController snackController = new SnackController();
		SnackAndChange snackAndChange = snackController.buyChips(transaction);
		
		if (snackAndChange.snack == null)
		{
			System.out.println("not enough coins");
		}
		else
		{
			System.out.println("snack is:" + snackAndChange.getSnack().getName());
			System.out.println("number of coins in change is:" + snackAndChange.getCoins().size());

		}	
		*/
		//test buying candy with mixed change
		Coin quarter = new Coin(24.26,5.67,0);
		Coin dime = new Coin(17.91,2.268,0);
		Coin nickel = new Coin(21.21,5,0);
		
		Transaction transaction = new Transaction();
		transaction.addCoins(quarter);
		transaction.addCoins(dime);
		transaction.addCoins(dime);
		transaction.addCoins(dime);
		transaction.addCoins(nickel);
		transaction.addCoins(nickel);
		transaction.addCoins(nickel);
		transaction.addCoins(nickel);
		
		SnackController snackController = new SnackController();
		SnackAndChange snackAndChange = snackController.buyCandy(transaction);
		
		if (snackAndChange.snack == null)
		{
			System.out.println("not enough coins");
		}
		else
		{
			System.out.println("snack is:" + snackAndChange.getSnack().getName());
			System.out.println("number of coins in change is:" + snackAndChange.getCoins().size());

		}
	}
}

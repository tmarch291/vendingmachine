package com.timsnas.vendingmachine.controllers;

import java.util.List;

import com.timsnas.vendingmachine.entities.Coin;
import com.timsnas.vendingmachine.entities.Snack;
import com.timsnas.vendingmachine.entities.SnackAndChange;

public class SnackController {
	
	//method for buying pop. returns snack and change in one object
	public SnackAndChange buyCola(Transaction transaction)
	{
		SnackAndChange snackAndChange = new SnackAndChange();
		
		if (transaction.getValue() >= 100)
		{
			Snack snack = new Snack();
			snack.setName("cola");
			List<Coin> coins = transaction.giveChange(100);
			snackAndChange.setSnack(snack);
			snackAndChange.setCoins(coins);
		}
		else
		{
			List<Coin> coins = transaction.giveChange(0);
			snackAndChange.setCoins(coins);
		}
		return snackAndChange;
	}
	
	//method for buying chips. returns snack and change in one object
	public SnackAndChange buyChips(Transaction transaction)
	{
		SnackAndChange snackAndChange = new SnackAndChange();
		
		if (transaction.getValue() >= 50)
		{
			Snack snack = new Snack();
			snack.setName("chips");
			List<Coin> coins = transaction.giveChange(50);
			snackAndChange.setSnack(snack);
			snackAndChange.setCoins(coins);
		}
		else
		{
			List<Coin> coins = transaction.giveChange(0);
			snackAndChange.setCoins(coins);
		}
		return snackAndChange;
	}
	
	//method for buying candy. returns snack and change in one object
	public SnackAndChange buyCandy(Transaction transaction)
	{
		SnackAndChange snackAndChange = new SnackAndChange();
		
		if (transaction.getValue() >= 65)
		{
			Snack snack = new Snack();
			snack.setName("candy");
			List<Coin> coins = transaction.giveChange(65);
			snackAndChange.setSnack(snack);
			snackAndChange.setCoins(coins);
		}
		else
		{
			List<Coin> coins = transaction.giveChange(0);
			snackAndChange.setCoins(coins);
		}
		return snackAndChange;
	}

}

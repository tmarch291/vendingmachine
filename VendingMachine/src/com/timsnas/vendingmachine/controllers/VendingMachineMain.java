package com.timsnas.vendingmachine.controllers;

import java.util.Scanner;

import com.timsnas.vendingmachine.entities.Coin;
import com.timsnas.vendingmachine.entities.SnackAndChange;

public class VendingMachineMain {
	
	public static void main(String[] args) {
		
		System.out.println("Welcome to the vending machine. Select either \"cola\" \"chips\" or \"candy\".");
		Scanner scanner = new Scanner(System.in);
		String snack = scanner.nextLine();
		System.out.println("Enter number of quarters:");
		int quarters = Integer.valueOf(scanner.nextLine());
		System.out.println("Enter number of dimes:");
		int dimes = Integer.valueOf(scanner.nextLine());
		System.out.println("Enter number of nickels:");
		int nickels = Integer.valueOf(scanner.nextLine());
		
		System.out.println("Requested:" + snack);
		System.out.println("Quarters entered:" + quarters);
		System.out.println("Dimes entered:" + dimes);
		System.out.println("Nickels entered:" + nickels);
		
		Transaction transaction = new Transaction();
		
		for (int i = 0; i < quarters; i++)
		{
			//every time the loop iterates, a quarter is added to the transaction
			Coin quarter = new Coin(24.26,5.67,0);
			transaction.addCoins(quarter);
		}
		
		for (int i = 0; i < dimes; i++)
		{
			//every time the loop iterates, a dime is added to the transaction
			Coin dime = new Coin(17.91,2.268,0);
			transaction.addCoins(dime);
		}
		
		for (int i = 0; i < nickels; i++)
		{
			//every time the loop iterates, a nickel is added to the transaction
			Coin nickel = new Coin(21.21,5,0);
			transaction.addCoins(nickel);
		}
		
		SnackController snackController = new SnackController();
		SnackAndChange snackAndChange = new SnackAndChange();
		if (snack.equals("cola"))
		{
			System.out.println("cola selected");
			snackAndChange = snackController.buyCola(transaction);
		}
		else if (snack.equals("chips"))
		{
			snackAndChange = snackController.buyChips(transaction);
		}
		else if (snack.equals("candy"))
		{
			snackAndChange = snackController.buyCandy(transaction);
		}
		else
		{
			System.out.println("Invalid Entry! Returning change!");
			snackAndChange.setCoins(transaction.giveChange(0));
		}
		
		if (snackAndChange.getSnack() != null)
		{
			System.out.println("Snack returned is:" + snackAndChange.getSnack().getName());
		}
		else
		{
			System.out.println("Not enough money. No snack returned.");
		}
		
		if (snackAndChange.getCoins() != null)
		{
			for (int i = 0; i < snackAndChange.getCoins().size(); i++)
			{
				System.out.println("Coin returned is worth:" + snackAndChange.getCoins().get(i).getValue());
			}
		}
	}

}

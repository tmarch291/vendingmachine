package com.timsnas.vendingmachine.controllers;

import java.util.LinkedList;
import java.util.List;

import com.timsnas.vendingmachine.entities.Coin;

public class Transaction {
	
	int transaction = 0;
	
	public void addCoins(Coin coin)
	{
		transaction += CoinValidator.getCoinValue(coin).getValue();
	}
	
	public List<Coin> giveChange(int purchasePrice)
	{
		List<Coin> coins = new LinkedList<Coin>();
		
		if (purchasePrice < transaction)
		{
			int change = transaction - purchasePrice;

			int numOfQuarters = change / 25;
			for (int i = 0; i < numOfQuarters; i++)
			{
				Coin coin = new Coin(24.26,5.67,25);
				coins.add(coin);
				change -= 25;
			}
			int numOfDimes = change / 10;
			for (int i = 0; i < numOfDimes; i++)
			{
				Coin coin = new Coin(17.91,2.268,10);
				coins.add(coin);
				change -= 10;
			}
			int numOfNickles = change / 5;
			for (int i = 0; i < numOfNickles; i++)
			{
				Coin coin = new Coin(21.21,5,5);
				coins.add(coin);
				change -= 5;
			}
			
			
		}
		
		return coins;
	}
	
	public int getValue()
	{
		return transaction;
	}

}

package com.timsnas.vendingmachine.controllers;

import com.timsnas.vendingmachine.entities.Coin;

public class CoinValidator {
	
	public static Coin getCoinValue(Coin coin)
	{
		
		//use greater than and less than to account for small variations in coin manufacture process and wear
		if ((coin.getDiameter() > 17.9 && coin.getDiameter() < 18) && (coin.getWeight() > 2.24 && coin.getWeight() < 2.3))
		{
			//it is a dime
			coin.setValue(10);
			return coin;
		}
		if ((coin.getDiameter() > 21.1 && coin.getDiameter() < 21.3) && (coin.getWeight() > 4.95 && coin.getWeight() < 5.05))
		{
			//it is a nickel
			coin.setValue(5);
			return coin;
		}
		if ((coin.getDiameter() > 24.21 && coin.getDiameter() < 24.31) && (coin.getWeight() > 5.62 && coin.getWeight() < 5.73))
		{
			//it is a quarter
			coin.setValue(25);
			return coin;
		}
		else
		{
			//returns coin with value of 0, essentially sending it to rejection pile
			return coin;
		}
	}

}
